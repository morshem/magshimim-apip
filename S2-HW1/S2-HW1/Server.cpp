#include <iostream>
#include <WinSock2.h>
#include <Windows.h>
#include <thread>

#pragma comment(lib,"ws2_32.lib")

using std::cin;
using std::cout;
using std::endl;
using std::thread;

void reply(SOCKET clientSock);

void main()
{
	SOCKET serverSock, clientSock = 0;
	WSADATA serverWSA;
	struct sockaddr_in serverInfo, clientInfo;
	char *acceptedMessage = "Accepted";

	//Init. serverWSA
	if (WSAStartup(MAKEWORD(2, 0), &serverWSA) != 0)
	{
		cout << "ERROR CODE: " << WSAGetLastError() << endl;
		return;
	}

	//Init. serverSock
	serverSock = socket(AF_INET, SOCK_STREAM, 0);
	if (serverSock == INVALID_SOCKET)
	{
		cout << "FATAL ERROR - CANNOT INITIATE SOCKET: " << WSAGetLastError() << endl;
		return;
	}

	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr.s_addr = INADDR_ANY;
	serverInfo.sin_port = htons(8888);

	//Bind IP & Port to socket
	if (bind(serverSock, (struct sockaddr*)&serverInfo, sizeof(serverInfo)) == SOCKET_ERROR)
	{
		cout << "FATAL ERROR - CANNOT BIND IP TO SOCKET: " << WSAGetLastError() << endl;
		return;
	}

	while(clientSock != INVALID_SOCKET)
	{
		listen(serverSock, 1);
		cout << "waiting for a client" << endl;
		int sockaddrSize = sizeof(struct sockaddr_in);
		clientSock = accept(serverSock, (struct sockaddr*)&clientInfo, &sockaddrSize);
		if (clientSock == INVALID_SOCKET)
		{
			cout << "FATAL ERROR - CANNOT ACCEPT SOCKET: " << WSAGetLastError() << endl;
			break;
		}
		cout << "Replying to a client connection" << endl;
		thread singleClientReply(reply, clientSock);
		singleClientReply.join();
	}

	closesocket(serverSock);
	WSACleanup();
	char stop;
	cin >> stop;
}

void reply(SOCKET clientSock)
{
	char* message = "Accepted";
	send(clientSock, message, strlen(message) + 1, 0);
	closesocket(clientSock);
}